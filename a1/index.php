<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php session_start(); ?>

    <?php if(!isset($_SESSION['email'])): ?>
        <form method="POST" name="action" value="login">
            Email: <input type="email" name="email" required>
            Password: <input type="password" name="password" required>
            <button type="submit">Login</button>
        </form>
        
    <?php if(error_message): ?>
        <p><?= $_SESSION['error_message']; ?></p>
        <?php unset($_SESSION['error_message']);
    <?php endif;?> 

    <?php else(): ?>
        <p>Hello, <?= $_SESSION['email']?>
            <form method="POST" name="action" value="logout">
            <input type="hidden" >  
            <button type="submit">Logout</button>
        </form>
    <?php endif; ?> 
        
</body>
</html>