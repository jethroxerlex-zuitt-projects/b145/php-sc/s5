<?php
$tasks = ['Get git', 'Bake HTML', 'Eat CSS', 'Learn PHP'];
if (isset($_GET['index'])) { //will check if it can GET any data passed via URL (specifically in this case, data labeled "index")
	$indexGet = $_GET['index'];//contains the value of "index"
	echo "The retrieved task from GET is $tasks[$indexGet].";//show the task with the same index chosen from the select below
}

if (isset($_POST['index'])) { //will check if it can GET any data passed via URL (specifically in this case, data labeled "index")
	$i = $_POST['index'];//contains the value of "index"
	echo "The retrieved task from POST is $tasks[$i].";//show the task with the same index chosen from the select below
}

//When submitting form data, the difference between GET and POST is that GET sends that data via the URL (which will be visible to the user), and POST does not (and is therefore more secure)

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S5: Client-Server Communication (GET and POST)</title>
</head>
<body>
	<h1>Task Index from GET</h1>
	<form method="GET">
		<select name="index" id="" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">GET</button>
	</form>

		<h1>Task Index from POST</h1>
	<form method="POST">
		<select name="index" id="" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">POST</button>
	</form>
</body>
</html>